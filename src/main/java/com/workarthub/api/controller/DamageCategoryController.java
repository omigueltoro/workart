package com.workarthub.api.controller;

import java.util.Comparator;
import java.util.List;
import com.workarthub.api.commons.IGenericRepository;
import com.workarthub.api.models.DamageCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class DamageCategoryController {
	
	@Autowired
	private IGenericRepository<DamageCategory> repository;
	
	@GetMapping(value="/damage-categories")
	public List<DamageCategory> getAll(){
		Comparator<DamageCategory> comparator = new Comparator<DamageCategory>() {
			@Override
			public int compare(DamageCategory left, DamageCategory right) {
				return left.getId() - right.getId();
			}
		};
		List<DamageCategory> list = repository.findAll();
		list.sort(comparator);

		return list;
	}
	
	@GetMapping(value="/damage-categories/{id}")
	public DamageCategory find(@PathVariable Integer id) {

		return repository.get(id);
	}
	@PostMapping(value="/damage-categories")
	public ResponseEntity<DamageCategory> save(@RequestBody DamageCategory damageClass){
		
		DamageCategory obj = repository.save(damageClass);
		return new ResponseEntity<DamageCategory>(obj, HttpStatus.OK);		
	}	
	
	@DeleteMapping(value="/damage-categories/{id}")
	public ResponseEntity<DamageCategory> delete(@PathVariable Integer id){
		
		DamageCategory damageClass = repository.get(id);
		if(damageClass != null) {
			repository.delete(damageClass);
		}else {
			return new ResponseEntity<DamageCategory>(damageClass, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<DamageCategory>(damageClass, HttpStatus.OK);
	}

}
