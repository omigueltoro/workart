package com.workarthub.api.controller;

import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.workarthub.api.commons.IGenericRepository;
import com.workarthub.api.models.DamageDepthLevel;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class DamageDepthLevelController {
	
	@Autowired
	private IGenericRepository<DamageDepthLevel> repository;
	
	@GetMapping(value="/damage-depth-levels")
	public List<DamageDepthLevel> getAll(){
		Comparator<DamageDepthLevel> comparator = new Comparator<DamageDepthLevel>() {
			@Override
			public int compare(DamageDepthLevel left, DamageDepthLevel right) {
				return left.getId() - right.getId();
			}
		};
		List<DamageDepthLevel> list = repository.findAll();
		list.sort(comparator);

		return list;
	}
	
	@GetMapping(value="/damage-depth-levels/{id}")
	public DamageDepthLevel find(@PathVariable final Integer id) {
		return repository.get(id);
	}
	@PostMapping(value="/damage-depth-levels")
	public ResponseEntity<DamageDepthLevel> save(@RequestBody final DamageDepthLevel damageDepthLevel){
		
		final DamageDepthLevel obj = repository.save(damageDepthLevel);
		return new ResponseEntity<DamageDepthLevel>(obj, HttpStatus.OK);		
		
	}	
	
	@DeleteMapping(value="/damage-depth-levels/{id}")
	public ResponseEntity<DamageDepthLevel> delete(@PathVariable final Integer id){
		
		final DamageDepthLevel damageDepthLevel = repository.get(id);
		if(damageDepthLevel != null) {
			repository.delete(damageDepthLevel);
		}else {
			return new ResponseEntity<DamageDepthLevel>(damageDepthLevel, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<DamageDepthLevel>(damageDepthLevel, HttpStatus.OK);	
		
	}

}
