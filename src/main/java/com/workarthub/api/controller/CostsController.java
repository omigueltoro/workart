package com.workarthub.api.controller;

import java.util.Comparator;
import java.util.List;
import com.workarthub.api.commons.IGenericRepository;
import com.workarthub.api.models.Cost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class CostsController {
	
	@Autowired
	private IGenericRepository<Cost> repository; 
	
	@GetMapping(value="/costs")
	public List<Cost> getAll(){
		Comparator<Cost> comparator = new Comparator<Cost>() {
			@Override
			public int compare(Cost left, Cost right) {
				return left.getId() - right.getId();
			}
		};
		List<Cost> list = repository.findAll();
		list.sort(comparator);

		return list;
	}
	
	@GetMapping(value="/costs/{id}")
	public Cost find(@PathVariable final Integer id) {
		return repository.get(id);
	}

	@PostMapping(value = "/costs")
	public ResponseEntity<Cost> save(@RequestBody Cost costs) {

		final Cost obj = repository.save(costs);
		return new ResponseEntity<Cost>(obj, HttpStatus.OK);

	}

	@DeleteMapping(value = "/costs/{id}")
	public ResponseEntity<Cost> delete(@PathVariable final Integer id) {

		final Cost cost = repository.get(id);
		if(cost != null) {
			repository.delete(cost);
		}else {
			return new ResponseEntity<Cost>(cost, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<Cost>(cost, HttpStatus.OK);	
		
	}

}
