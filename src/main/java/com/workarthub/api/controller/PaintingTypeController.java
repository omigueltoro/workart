package com.workarthub.api.controller;

import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.workarthub.api.commons.IGenericRepository;
import com.workarthub.api.models.PaintingType;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class PaintingTypeController {
	
	@Autowired
	private IGenericRepository<PaintingType> repository;
	
	@GetMapping(value="/painting-types")
	public List<PaintingType> getAll(){
		Comparator<PaintingType> comparator = new Comparator<PaintingType>() {
			@Override
			public int compare(PaintingType left, PaintingType right) {
				return left.getId() - right.getId();
			}
		};
		List<PaintingType> list = repository.findAll();
		list.sort(comparator);

		return list;
	}
	
	@GetMapping(value="/painting-types/{id}")
	public PaintingType find(@PathVariable Integer id) {

		PaintingType paintingType = repository.get(id);

		return paintingType;
	}
	@PostMapping(value="/painting-types")
	public ResponseEntity<PaintingType> save(@RequestBody PaintingType painting){
		
		PaintingType obj = repository.save(painting);
		return new ResponseEntity<PaintingType>(obj, HttpStatus.OK);		
		
	}	
	
	@DeleteMapping(value="/painting-types/{id}")
	public ResponseEntity<PaintingType> delete(@PathVariable Integer id){
		
		PaintingType painting = repository.get(id);
		if(painting != null) {
			repository.delete(painting);
		}else {
			return new ResponseEntity<PaintingType>(painting, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<PaintingType>(painting, HttpStatus.OK);	
		
	}	

}
