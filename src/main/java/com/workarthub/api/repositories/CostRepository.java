/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.workarthub.api.repositories;
import com.workarthub.api.commons.IGenericRepository;
import com.workarthub.api.models.Cost;
import org.springframework.stereotype.Repository;
/**
 *
 * @author wancho
 */
@Repository 
public interface CostRepository extends IGenericRepository <Cost> {

}
