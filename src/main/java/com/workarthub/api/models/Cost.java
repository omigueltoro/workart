package com.workarthub.api.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Cost")
public class Cost {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	@Column(name="cost", length = 50)
	double cost;
	
	
	@ManyToOne
	@JoinColumn(name = "PaintingType", referencedColumnName = "Id", nullable = false)
	private PaintingType paintingType;
	
	@ManyToOne
	@JoinColumn(name = "DamageCategory", referencedColumnName = "Id", nullable = false)
	private DamageCategory damageCategory;
	
	@ManyToOne
	@JoinColumn(name = "DamageDepthLevel", referencedColumnName = "Id", nullable = false)
	private DamageDepthLevel damageDepthLevel;
	
	
	public int getId() {
		return id;
	}

	public void setId(int costid) {
		this.id = costid;
	}

	public PaintingType getPaintingType() {
		return paintingType;
	}

	public void setPaintingType(PaintingType paintingType) {
		this.paintingType = paintingType;
	}

	public DamageCategory getDamageCategory() {
		return damageCategory;
	}

	public void setDamageCategory(DamageCategory damageCategory) {
		this.damageCategory = damageCategory;
	}

	public DamageDepthLevel getDamageDepthLevel() {
		return damageDepthLevel;
	}

	public void setDamageDepthLevel(DamageDepthLevel damageDepthLevel) {
		this.damageDepthLevel = damageDepthLevel;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	} 
}
