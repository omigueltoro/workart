package com.workarthub.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages="com.workarthub.api.repositories")
public class WorkArtApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkArtApplication.class, args);
	}

}
